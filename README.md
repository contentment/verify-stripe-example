# Stripe Integration for Padify #

This is an example of a bare bone implementation of stripe payments for Padify publications.

Please refer to the following documentation resources for a more in depth explanation:

- [Padify Developer documentation](http://developers.padify.net)
- [Stripe.js documentation](https://stripe.com/docs/stripe.js)