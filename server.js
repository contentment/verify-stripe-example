var log = console.log.bind(console);
var fs = require('mz/fs');
var Promise = require('bluebird');

var app = require('express')();
var request = require('superagent');

var db = require('monk')(process.env.MONGOHQ_URL || 'localhost/example');

app.use(require('morgan')('tiny'));
app.use(require('body-parser')());

app.get('/', function (req, res, next) {
  db.get('stripe').find().then(function (o) {
    return tpl('./index.html', {list: JSON.stringify(o)});
  }, next)
  .then(function(str) {
    res.type('html').send(str);
  }, next)
});
app.get('/app.js', serve('./app.js', 'js'));

var pixel = new Buffer( '\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\xf0\x01\x00\xff\xff\xff\x00\x00\x00\x21'+
                        '\xf9\x04\x01\x0a\x00\x00\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02\x44\x01'+
                        '\x00\x3b');
app.head('/favicon.ico', function (req, res) {
  res.send(304);
})
app.get('/favicon.ico', function(req, res) {
  res.set({
    'Content-Type':'image/gif',
    'Cache-Control': 'private, no-cache, proxy-revalidate',
    'Content-Disposition': 'inline',
    'Content-Length': '43'
  });
  res.end(pixel);
});

// Charge, single product, endpoint
app.post('/charge', function (req, res, next) {
  request
  .post('https://api.stripe.com/v1/charges')
  .auth('sk_test_zeranbUIgvgOsyjHBulBi6xZ','')
  .set('content-type', 'application/x-www-form-urlencoded')
  .send({
    amount: 2000
  , currency: 'gbp'
  , card: req.param('stripeToken')
  , metadata: {
      mail: req.param('stripeEmail')
    , app: 'io.banana.app'
    , product: 'io.banana.app.banana'
    }
  })
  .end(function (err, resp) {
    if (resp.body.error) {
      return res.json(resp.body);
    }

    var o = {
      t: resp.body.object
    , sid: resp.body.id
    , amount: resp.body.amount
    , app: resp.body.metadata.app
    , mail: resp.body.metadata.mail
    , product: resp.body.metadata.product
    , provider: 'stripe'
    }

    db.get('stripe')
    .insert(o)
    .then(function (obj) {
      res.redirect('/' + obj.sid);
    });
  });
});

// Subscription endpoint
app.post('/sub', function (req, res, next) {
  request
  .post('https://api.stripe.com/v1/customers')
  .auth('sk_test_zeranbUIgvgOsyjHBulBi6xZ','')
  .set('content-type', 'application/x-www-form-urlencoded')
  .send({
    card: req.param('stripeToken')
  , email: req.param('stripeEmail')
  , plan: 'com.getcontentment.test.sub2'
  , metadata: {
      app: 'io.banana.app'
    , product: 'com.getcontentment.test.sub2'
    }
  })
  .end(function (err, resp) {
    if (resp.body.error) {
      return res.json(resp.body);
    }

    var o = {
      t:     resp.body.object
    , sid:   resp.body.id
    , app:   resp.body.metadata.app
    , mail:  resp.body.email
    , body:  resp.body
    , product: 'com.getcontentment.test.sub2'
    , provider: 'stripe'
    }

    db.get('stripe')
    .insert(o)
    .then(function (obj) {
      res.redirect('/' + obj.sid);
    }, next);
  })
});

app.get('/:sid', function (req, res, next) {
  db.get('stripe')
  .findOne({sid: req.param('sid')})
  .then(function (obj){
    return tpl('./charge.html', obj);
  }, next)
  .then(function (str) {
    res.type('html').send(str);
  });
})

app.listen(process.env.PORT || 4321);

// send file middleware, don't try this at home
function serve (filename, t) {
  var cache = '';
  return function (req, res, next) {
    res.type(t)

    if (cache.length > 0) {
      // hopefully does its job
      return res.send(cache);
    }

    fs.readFile(filename)
    .then(function (data) {
      cache = data.toString();
      res.send(cache);
    })
    .catch(res.send.bind(res))
  }
}

var _tpl = {};
function tpl (file, obj) {
  return (
    !_tpl[file]
    ? fs.readFile(file)
    : Promise.fulfill(_tpl[file])
  )
  .then(function (str) {
    str = str.toString();
    obj.obj = JSON.stringify(obj);
    Object.keys(obj).forEach(function (k) {
      str = str.replace(new RegExp('{{'+ k +'}}', 'gi'), obj[k]);
    });
    return str;
  });
}

